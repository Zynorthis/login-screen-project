For this project the following goals are what I'd like to have this program accomplish:

[] Have a screen that allows you to select between three options, "login", "create new account", and "exit"
[] Have a screen that will allow you to enter a username and password that is then saved (password varification also needed were
	you need to enter the same password twice as in the creation screen to varify that you entered it correctly)
[] Have all usernames being created doubled checked in the "Locked accounts" folder before allowing creation
[] Have a screen that allows you to enter saved usernames and passwords
[] If a username was entered incorrectly then display " -- is not a valid username" were -- is the entered username
[] If the username is correctly entered but the password is not, display a message and also allow them 3 more trys
	at entering the password correctly before moving the account to a different folder labled "Lockecd accounts"
	and displaying a message
[] Entering in a valid saved username and password will bring you to a screen that displays a link were the code is
	uploaded to and also thanks the user for testing the program

~~~~~~~~~~~~~~~~

For personal gain, this will also help me develop my own "style of code" and will also help me better understand what
I've already learned. Adding more content to my portfolio is nice too. :p